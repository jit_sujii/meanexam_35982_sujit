const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
//const jwt = require('jsonwebtoken')



const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))


 
const SigninRouter = require('./admin/routes/signin')
 
//routes
app.post('/signin', SigninRouter)
 


app.get('/', (request,response)=>{
    response.send('welcome to my casestudy application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('admin-server started on port 3000')
})